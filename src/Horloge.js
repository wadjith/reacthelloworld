import React from "react";
import DayOfWeek from "./DayofWeek";
import DateFormated from "./DateFormated";
import Time from "./Time";
import MomentOfHour from "./MomentOfHour";

class Horloge extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            myDate: new Date(),
            textYear: "",
            textMonth: "",
            textDay: "",
            textHour: "",
            textMinute: "",
        };
        this.handleChangeYear   = this.handleChangeYear.bind(this);
        this.handleChangeMonth  = this.handleChangeMonth.bind(this);
        this.handleChangeDay    = this.handleChangeDay.bind(this);
        this.handleChangeHour   = this.handleChangeHour.bind(this);
        this.handleChangeMinute = this.handleChangeMinute.bind(this);
        this.handleSubmit       = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        this.clock(new Date());
    }
    componentWillUnmount() {
        clearInterval(this.timerID)
    }

    clock(theDate){
        this.setState({myDate: theDate});
        this.timerID = setInterval(() => this.setState(state =>({
            myDate: new Date(state.myDate.setSeconds(state.myDate.getSeconds() + 1)),
        })), 1000);
    }

    handleChangeYear(e) {
    this.setState({ textYear: e.target.value });
  }
      handleChangeMonth(e) {
        this.setState({ textMonth: e.target.value });
      }
      handleChangeDay(e) {
        this.setState({ textDay: e.target.value });
      }
      handleChangeHour(e) {
        this.setState({ textHour: e.target.value });
      }
      handleChangeMinute(e) {
        this.setState({ textMinute: e.target.value });
      }

      handleSubmit(e) {
        e.preventDefault();
        let theDate;
        let year = parseInt(this.state.textYear);
        let month = parseInt(this.state.textMonth);
        let day = parseInt(this.state.textDay);
        let hour = parseInt(this.state.textHour);
        let minute = parseInt(this.state.textMinute);
        if (year && month && day && hour && minute)
            theDate = new Date(year, month-1, day, hour, minute)
            else if(year && month && day && hour)
                theDate = new Date(year, month-1, day, hour)
            else if(year && month && day)
                theDate = new Date(year, month-1, day);
            else if(year && month)
                theDate = new Date(year, month-1)
            else
                theDate = new Date();
        this.clock(theDate);

    }

    render() {
        let day = this.state.myDate.getDay();
        let hour = this.state.myDate.getHours();
        let theTime = this.state.myDate.toLocaleTimeString();
        let dateFormated = this.state.myDate.toDateString();
        return(
            <div>
                <DayOfWeek day = { day } />
                <MomentOfHour hour = { hour } />
                <Time time = { theTime } />
                <DateFormated theDate = { dateFormated } />
                <form onSubmit={this.handleSubmit}>
                <label htmlFor="year">Année</label>
                  <input type="number" id="year" min="1000"
                         onChange={this.handleChangeYear} value={this.state.textYear} />
                <label htmlFor="month">Mois</label>
                  <input type="number" id="month" min="1" max="12"
                         onChange={this.handleChangeMonth} value={this.state.textMonth} />
                <label htmlFor="day">Jour</label>
                  <input type="number" id="day" min="1" max="31"
                         onChange={this.handleChangeDay} value={this.state.textDay}  />
                <label htmlFor="hour">Heure</label>
                  <input type="number" id="hour" min="1" max="23"
                         onChange={this.handleChangeHour}  value={this.state.textHour}  />
                <label htmlFor="minute">Minutes</label>
                  <input type="number" id="minute" min="1" max="59"
                         onChange={this.handleChangeMinute}  value={this.state.textMinute}  />
                  <button>
                    Modifier la Date
                  </button>
                </form>
            </div>

        );
    }
}
export default Horloge;