import logo from './logo.svg';
import './App.css';
import Horloge from "./Horloge";

function App() {
     let logoImage = <img src={logo} className="App-logo" alt="logo" />;
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Hello World from Thierry WADJI!!
        </p>
          <Horloge />
      </header>

    </div>
  );
}

export default App;
