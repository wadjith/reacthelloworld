function MomentOfHour(props){
    const { hour } = props;
    let moment = '';
    if (5 <= hour && hour <= 12)  {
        moment = "MATIN";
    } else if (12 < hour && hour < 18) {
        moment = "APRES-MIDI"
    } else if (18 <= hour && hour <= 23) {
        moment = "SOIR"
    } else {
        moment = "PETIT-MATIN"
    }

    return(
        <h2>{ moment }</h2>
    );
}
export default MomentOfHour;